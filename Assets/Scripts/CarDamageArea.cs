﻿using UnityEngine;
using System.Collections;

public class CarDamageArea : MonoBehaviour {

	public int damageAmount;
	public GameObject player;


	void OnTriggerEnter2D(Collider2D other){
		if (other.CompareTag ("Ground")) {
			player.GetComponent<Player> ().TakeDamage(damageAmount);
		}
	}


}
