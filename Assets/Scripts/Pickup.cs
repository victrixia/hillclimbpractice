﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {

	public int ScoreAmount = 1;

	public AudioClip pickupSound;


	private void OnTriggerEnter2D(Collider2D other){
		if (other.CompareTag ("Player")) {

			other.GetComponent<Player> ().PickUp (ScoreAmount);

			GameManager._instance.PlaySound (pickupSound);

			Destroy (this.gameObject);
			
		}
	}

}
