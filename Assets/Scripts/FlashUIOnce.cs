﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Used to flash an CanvasGroup or an Text element once. If called again while flashing, just updates the text and continues as normal.
/// </summary>
public class FlashUIOnce : MonoBehaviour {

    public float stayTime;
    public float speed;
    public Text textToEdit;
    public Text secondaryText;
    public CanvasGroup groupToEdit;
    public bool deleteWhenOver = false;
    string status = "idle";

    public void flashText(string newText)
    {
        if (textToEdit != null && status == "idle")
        {
            textToEdit.text = newText;
            status = "fadeInElement";

            if (secondaryText != null)
                secondaryText.text = newText;

            StartCoroutine(flashElement());
        }
        else if (textToEdit != null)
        {
            textToEdit.text = newText;
            if (secondaryText != null)
                secondaryText.text = newText;
        }
    }

    public void flashText(string newText, Color textColor)
    {
        textToEdit.color = textColor;
        flashText(newText);
    }

    public void flashGroup(string newText)
    {
        if (textToEdit != null && groupToEdit != null && status == "idle")
        {
            textToEdit.text = newText;
            status = "fadeInGroup";

            if (secondaryText != null)
                secondaryText.text = newText;

            StartCoroutine(flashGroup());
        }
        else if (textToEdit != null && groupToEdit != null)
        {
            textToEdit.text = newText;
            if (secondaryText != null)
                secondaryText.text = newText;
        }
    }

    public void flashGroup(string newText, Color textColor)
    {
        textToEdit.color = textColor;
        flashGroup(newText);        
    }

    public float calculateHowLongForLoopToComplete()
    {
        return (1 / (speed * Time.deltaTime) * Time.deltaTime) + stayTime;
    }

    IEnumerator flashGroup()
    {
        while (groupToEdit.alpha != 1f)
        {
            if (groupToEdit.alpha + speed * Time.deltaTime < 1f)
                groupToEdit.alpha = groupToEdit.alpha + speed * Time.deltaTime;
            else
                groupToEdit.alpha = 1f;

            yield return null;
        }

        yield return new WaitForSeconds(stayTime);

        while (groupToEdit.alpha != 0f)
        {
            if (groupToEdit.alpha - speed * Time.deltaTime > 0f)
                groupToEdit.alpha = groupToEdit.alpha - speed * Time.deltaTime;
            else
                groupToEdit.alpha = 0f;

            yield return null;
        }

        status = "idle";

        if (deleteWhenOver)
            Destroy(gameObject);
    }

    IEnumerator flashElement()
    {
        while (textToEdit.color.a != 1f)
        {
            if (textToEdit.color.a + speed * Time.deltaTime < 1f)
                textToEdit.color = new Color(textToEdit.color.r, textToEdit.color.g, textToEdit.color.b, textToEdit.color.a + speed * Time.deltaTime);
            else
                textToEdit.color = new Color(textToEdit.color.r, textToEdit.color.g, textToEdit.color.b, 1f);

            yield return null;
        }

        yield return new WaitForSeconds(stayTime);

        while (textToEdit.color.a != 0f)
        {
            if (textToEdit.color.a - Time.deltaTime / speed > 0f)
                textToEdit.color = new Color(textToEdit.color.r, textToEdit.color.g, textToEdit.color.b, textToEdit.color.a - speed * Time.deltaTime);
            else
                textToEdit.color = new Color(textToEdit.color.r, textToEdit.color.g, textToEdit.color.b, 0f);

            yield return null;
        }

        status = "idle";
        if (deleteWhenOver)
            Destroy(gameObject);
    }

}
