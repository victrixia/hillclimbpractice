﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; 
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager _instance;

	private int score=0;

	public Text scoreText;
	public GameObject finishText, gameOverText;

	public AudioSource source;
	private bool finished;


	void Awake(){
		_instance = this;
	}

	void Start(){
		AddScore (0);
		Time.timeScale = 1f;
		finished = false;
	}

	void Update(){

		if (finished) {
			if (Input.GetKeyDown (KeyCode.Space)) {
				Restart ();
			}
		}

	}

	public void AddScore(int amount){
		score += amount;
		scoreText.text = "Score: " + score;
	}

	public void Restart(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

	}

	public void Finished(){
		EndGame (finishText);
	}

	public void GameOver(){
		EndGame (gameOverText);
	}

	void EndGame(GameObject obj){
		finished = true;

		obj.SetActive (true);

		Time.timeScale = 0f;
	}

	public void PlaySound(AudioClip _audio){
	
		if (_audio != null) {
			source.PlayOneShot(_audio);
		}
	}


}
