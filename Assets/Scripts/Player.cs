﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	public int health;
	private int maxHealth;
	public Slider healthSlider;

	void Start () {
		maxHealth = health;
	}

	public void PickUp(int _amount){
		
		GameManager._instance.AddScore (_amount);

	}

	public void TakeDamage(int amount){
		health -= amount;

		if (health < 0) {
			health = 0;

			GameManager._instance.GameOver ();
		}

		healthSlider.value = health;


	}
}
