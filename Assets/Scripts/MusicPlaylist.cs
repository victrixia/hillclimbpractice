﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Used to run a playlist of music that can be shuffled. 
/// </summary>
public class MusicPlaylist : MonoBehaviour {

    public bool debugOn = false;
    public AudioSource musicSource;
    public AudioSource musicSource2;
    public bool shuffle;
    public float fadeTime;
    public bool playOnStart;
    public bool showChangeOnUI;
    public string preTrackInfoText;
    public FlashUIOnce uiElementToFlash;
    
    public List<musicTrack> playList;
    List<musicTrack> currentPlaylist;
    List<musicTrack> specialPlaylist;
    bool specialTrack = false;

    bool changeTrack = false;
    public bool ChangeTrack
    {
        get
        {
            return changeTrack;
        }
    }

    bool fadeOut = false;
    public bool FadeOut
    {
        get
        {
            return fadeOut;
        }
    }   

    float fadeInStep = 0;
    float fadeOutStep = 0;
    float targetVolume = 1;
    AudioSource currentMusicSource;
    AudioClip lastTrack;

    // Use this for initialization
    void Start()
    {
        if (musicSource == null)
            musicSource = gameObject.AddComponent<AudioSource>();

        if (musicSource2 == null)
            musicSource2 = gameObject.AddComponent<AudioSource>();

        generateNewPlaylist();

        if (playOnStart)
            changeMusicTrack(fadeTime);
    }

    // Update is called once per frame
    void Update()
    {       
        if (changeTrack)
        {
            if (currentMusicSource == musicSource)
            {
                if (musicSource.volume + fadeInStep <= targetVolume)
                    musicSource.volume += fadeInStep;
                else if (musicSource.volume + fadeInStep > targetVolume)
                    musicSource.volume = targetVolume;

                if (musicSource2.volume - fadeOutStep >= 0)
                    musicSource2.volume -= fadeOutStep;
                else if (musicSource2.volume - fadeOutStep < 0)
                    musicSource2.volume = 0;

                if (musicSource.volume == targetVolume && musicSource2.volume == 0)
                {
                    if (musicSource2.clip != null && musicSource.clip != null && debugOn)
                        Debug.Log("Changed " + musicSource.clip.name + " on and faded out " + musicSource2.clip.name + ".");
                    else if (debugOn && musicSource.clip != null)
                        Debug.Log("Changed to: " + musicSource.clip.name + ".");

                    changeTrack = false;
                    musicSource2.Stop();
                }
            }
            else if (currentMusicSource == musicSource2)
            {
                if (musicSource2.volume + fadeInStep <= targetVolume)
                    musicSource2.volume += fadeInStep;
                else if (musicSource2.volume + fadeInStep > targetVolume)
                    musicSource2.volume = targetVolume;

                if (musicSource.volume - fadeOutStep >= 0)
                    musicSource.volume -= fadeOutStep;
                else if (musicSource.volume - fadeOutStep < 0)
                    musicSource.volume = 0;

                if (musicSource2.volume == targetVolume && musicSource.volume == 0)
                {
                    if (musicSource2.clip != null && musicSource.clip != null && debugOn)
                        Debug.Log("Changed " + musicSource2.clip.name + " on and faded out " + musicSource.clip.name + ".");
                    else if (debugOn && musicSource2.clip != null)
                        Debug.Log("Changed to: " + musicSource2.clip.name + ".");

                    changeTrack = false;
                    musicSource.Stop();
                }
            }
        }
        else if (currentMusicSource != null)
        {
            if (fadeTime > 0.1f && (currentMusicSource.clip.length - currentMusicSource.time < fadeTime && !specialTrack))
                changeMusicTrack(fadeTime);
            else if (currentMusicSource.clip.length - currentMusicSource.time < 0.1f)
                changeMusicTrack();
        }

        if (fadeOut)
        {
            if (currentMusicSource.volume - fadeOutStep >= 0)
                currentMusicSource.volume -= fadeOutStep;
            else if (currentMusicSource.volume - fadeOutStep < 0)
                currentMusicSource.volume = 0;

            if (currentMusicSource.volume == 0)
            {
                if (debugOn)
                    Debug.Log("Fade out done.");

                fadeOut = false;
                currentMusicSource.Stop();
            }
        }
    }

    /// <summary>
    /// Immediately starts playing the given floor clip with no fade in or out.
    /// </summary>
    public void changeMusicTrack()
    {
        if (currentMusicSource == null)
            currentMusicSource = musicSource;

        if (currentPlaylist.Count > 1)
        {
            currentPlaylist.Remove(currentPlaylist[0]);
            currentMusicSource.clip = currentPlaylist[0].clipToPlay;            
        }
        else
        {
            generateNewPlaylist();
            currentMusicSource.clip = currentPlaylist[0].clipToPlay;
        }

        currentMusicSource.volume = currentPlaylist[0].trackVolume;
        currentMusicSource.Play();

        if (debugOn)
            Debug.Log("Instantly changed clip to " + currentMusicSource.clip.name + ".");

        flashUIElementWithText(preTrackInfoText + currentPlaylist[0].artistName + " - " + currentPlaylist[0].trackName);
    }

    /// <summary>
    /// Starts the fade to new music track in the given time, and if there is any other music playing, they will faded out in the same time.
    /// </summary>
    /// <param name="fadeTime">Amount of time for the fadeIn in seconds.</param>
    public void changeMusicTrack(float fadeTime)
    {
        if (fadeTime > 0)
        {
            if (currentMusicSource == null)
                currentMusicSource = musicSource;

            if (musicSource.isPlaying && !musicSource2.isPlaying)
            {
                musicSource2.volume = 0;

                if (currentPlaylist.Count > 1)
                    currentPlaylist.Remove(currentPlaylist[0]);
                else
                {
                    lastTrack = currentMusicSource.clip;
                    generateNewPlaylist();                    
                }

                musicSource2.clip = currentPlaylist[0].clipToPlay;
                targetVolume = currentPlaylist[0].trackVolume;
                currentMusicSource = musicSource2;
                fadeOutStep = musicSource.volume / (fadeTime * (1f / Time.deltaTime));
                musicSource2.Play();
            }
            else
            {
                musicSource.volume = 0;

                if (currentPlaylist.Count > 1)
                    currentPlaylist.Remove(currentPlaylist[0]);
                else
                {
                    lastTrack = currentMusicSource.clip;
                    generateNewPlaylist();
                }

                musicSource.clip = currentPlaylist[0].clipToPlay;
                targetVolume = currentPlaylist[0].trackVolume;  
                currentMusicSource = musicSource;
                fadeOutStep = musicSource2.volume / (fadeTime * (1f / Time.deltaTime));
                musicSource.Play();
            }

            fadeInStep = currentPlaylist[0].trackVolume / (fadeTime * (1f / Time.deltaTime));

            changeTrack = true;

            if (debugOn)
                Debug.Log("Changing clip to " + currentMusicSource.clip.name + ".");

            flashUIElementWithText(preTrackInfoText + currentPlaylist[0].artistName + " - " + currentPlaylist[0].trackName);
        }
        else
            changeMusicTrack();
    }

    /// <summary>
    /// Used to just fade out the current track, if there is anything playing.
    /// </summary>
    /// <param name="fadeTime">Fade out time in seconds.</param>
    public void fadeOutCurrent(float fadeTime)
    {
        if ((musicSource.isPlaying || musicSource2.isPlaying) && currentMusicSource.volume > 0)
        {
            if (fadeTime > 0)
            {
                fadeOut = true;
                fadeOutStep = currentMusicSource.volume / (fadeTime * (1f / Time.deltaTime));

                if (debugOn)
                    Debug.Log("Starting fadeout.");
            }
            else
            {
                currentMusicSource.volume = 0;
                currentMusicSource.Stop();
            }
        }
    }

    /// <summary>
    /// Used to pause the current track.
    /// </summary>
    public void pauseCurrent()
    {
        if (currentMusicSource != null)
            currentMusicSource.Pause();
    }

    /// <summary>
    /// Used to unpause the currently paused track and fade it back in.
    /// </summary>
    public void unPauseCurrentWithFade()
    {
        if (currentMusicSource == null)
            changeBackToNormalPlaylist();
        else
        {
            currentMusicSource.UnPause();

            if (fadeTime > 0)
            {
                currentMusicSource.volume = 0;
                changeTrack = true;
            }            
        }

        if (debugOn)
            Debug.Log("Starting fade back in.");
    }

    public bool CheckIfSomethingIsPlaying()
    {
        if (musicSource.isPlaying || musicSource2.isPlaying)
            return true;
        else
            return false;
    }

    public void changePlaylist(List<musicTrack> newPlaylist)
    {
        playList = newPlaylist;
        generateNewPlaylist();

        if (lastTrack != null)
            changeMusicTrack(fadeTime);
        else
            changeMusicTrack();
    }

    /// <summary>
    /// Starts playing the special playlist instead of the normal one.
    /// </summary>
    /// <param name="newSpecialPlaylist">special playlist to play</param>
    /// <param name="fadeTime"></param>
    public void playSpecialPlaylist(List<musicTrack> newSpecialPlaylist)
    {
        specialPlaylist = newSpecialPlaylist;
        specialTrack = true;
        currentPlaylist.Clear();
        foreach (musicTrack clippy in specialPlaylist) { currentPlaylist.Add(clippy); }
        targetVolume = currentPlaylist[0].trackVolume;        

        if (fadeTime > 0)
        {
            if (musicSource.isPlaying && !musicSource2.isPlaying)
            {
                musicSource2.volume = 0;                
                musicSource2.clip = currentPlaylist[0].clipToPlay;
                currentMusicSource = musicSource2;  
                fadeOutStep = musicSource.volume / (fadeTime * (1f / Time.deltaTime));                              
                musicSource2.Play();
            }
            else
            {       
                musicSource.volume = 0;
                musicSource.clip = currentPlaylist[0].clipToPlay;
                currentMusicSource = musicSource;
                fadeOutStep = musicSource2.volume / (fadeTime * (1f / Time.deltaTime));
                musicSource.Play();
            }

            fadeInStep = currentPlaylist[0].trackVolume / (fadeTime * (1f / Time.deltaTime));
            changeTrack = true;

            if (debugOn)
                Debug.Log("Changing clip to " + currentMusicSource.clip.name + ".");

            flashUIElementWithText(preTrackInfoText + currentPlaylist[0].artistName + " - " + currentPlaylist[0].trackName);
        }
        else
            changeMusicTrack();
    }

    public void changeBackToNormalPlaylist()
    {
        specialTrack = false;
        generateNewPlaylist();
        changeMusicTrack(fadeTime);
    }
    
    public void flashUIElementWithText(string textToShow)
    {
        if (showChangeOnUI && uiElementToFlash != null)
            uiElementToFlash.flashGroup(textToShow);
    }

    void generateNewPlaylist()
    {
        currentPlaylist = new List<musicTrack>();
        if (!specialTrack)
        {
            if (shuffle)
            {
                List<int> trackIndexes = new List<int>();
                for (int i = 0; i < playList.Count; i++) { trackIndexes.Add(i); }

                int randomIndex = Random.Range(0, trackIndexes.Count);
                while (playList[randomIndex].clipToPlay == lastTrack) { randomIndex = Random.Range(0, trackIndexes.Count); }

                for (int i = 1; i < trackIndexes.Count; i++)
                {
                    int temp = trackIndexes[i];
                    randomIndex = Random.Range(i, trackIndexes.Count);
                    trackIndexes[i] = trackIndexes[randomIndex];
                    trackIndexes[randomIndex] = temp;
                }

                foreach (int trackIndex in trackIndexes) { currentPlaylist.Add(playList[trackIndex]); }
            }
            else
                foreach (musicTrack clippy in playList) { currentPlaylist.Add(clippy); }
        }
        else        
            foreach (musicTrack clippy in specialPlaylist) { currentPlaylist.Add(clippy); }        
    }

}

[System.Serializable]
public class musicTrack
{
    public string trackName;
    public string artistName;
    public AudioClip clipToPlay;
    public float trackVolume;
}
