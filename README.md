# README #

### HillClimb-harjoitustyö ###

* Elikkäs yhdessä nyt harjoitellaan tätä versionhallintaa ja muokkaillaan annettua pelipohjaa.
* Markdown on tekstimuotoilu mitä tässä Readmessa käytetään, jos haluatte tähän jotain lisätä. 
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Tähän ohjeet pelin asentamiseksi

### Contribution guidelines ###

* Täältä löydät ohjeita versionhallinnan käyttöön ja omien muutoksien lisäämiseen. [Ohjeet](https://docs.google.com/document/d/1uxvKnTTwgoF3NyyBvI2FBB_CSFSP_YJt4DUuD6gUKBw/edit?usp=sharing)
* Meillä on myös slack: [Tämä linkki on voimassa 12.3. asti](https://kivisetjasorasettuas.slack.com/shared_invite/MTQ5OTE4MDM0Njc2LTE0ODg3MjA5OTAtNDJkODkwOGYyNg)
* Trello löytyy täältä: [Kiviset ja Soraset -Trello](https://trello.com/b/tMXvChBN/kiviset-ja-soraset)

### Who do I talk to? ###

* Jos tulee hätä, hihkaise Alixille Facebookissa!